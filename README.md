Here are some of my public portfolios.<br /><br />

All digital product below including all aspects within are produced end-to-end by me and the team under my lead. Product/service research involving cross functional departments and then we handle the complete design process until deployment/publication. Including maintenance, performance monitoring, reviews, and optimisation.<br /><br />

Desktop Web:<br />
https://www.astadala.com/<br /><br />

Mobile Web:<br />
https://m.astadala.com/<br />
https://m.villaseminyak.com/<br />
https://m.theseminyaksuite.com/<br />
https://m.lagoonspaseminyak.com/<br /><br />

Responsive Web:<br />
https://www.thehaereseminyak.com/<br />
https://www.thebenehotel.com/<br />
https://www.restaurantpencar.com/<br /><br />

Landing Page:<br />
https://www.villaseminyak.com/day-pass-bali<br />
https://www.chillseminyak.com/reflexology<br /><br />

Membership App:<br />
https://member.astadala.com<br /><br />

Campaign Videos:<br />
https://www.youtube.com/watch?v=rbKzdRcK3to<br />
https://www.youtube.com/watch?v=ZPfrhvVmaIQ<br /><br />

Instagram:<br />
https://www.instagram.com/villaseminyakestate/<br />
https://www.instagram.com/theseminyaksuite/<br />
https://www.instagram.com/lagoonspaseminyak/<br />
https://www.instagram.com/thebenehotel/<br />



